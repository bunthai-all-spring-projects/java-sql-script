package com.example.java_sql_script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.io.BufferedInputStream;
import java.io.FileReader;
import java.sql.PreparedStatement;


/**
 * it's difficult to do sql migration script with java
 * - execute line by line not the hold file
 * - handle comment (delimiter)
 */
@SpringBootApplication
public class JavaSqlScriptApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSqlScriptApplication.class, args);
	}

}
