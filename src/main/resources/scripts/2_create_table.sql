CREATE SEQUENCE testtable_id_seq;
CREATE TABLE testtable (
   id BIGINT PRIMARY KEY DEFAULT NEXTVAL('testtable_id_seq'),
   description VARCHAR(250)
);